- Database Server Health Check
- Only Execute to Get the Information from CLI
- Other Information will be captured through Percona Monitoring Management Dashboard

### Need to Change
- Remote Client Hostname and IP Address in Inventory File
- In vars File, Change the Username and Password to Access the Database in Order to Get the Information
- Change the remote user when SSH to the remote node ansible.cfg
- Change the password to have the sudo privelege in inventory
- Change the path where to store the output in the config

### Run ansible-playbook with ask become password
### Use this if ssh to the target server using specific username
- ansible-playbook main.yml -u [username] --ask-pass --ask-become-pass 

### Variables to run the ansible
- Use _vars_ file to store the variables and run the ansible without prompting after run the ansible
- Use _vars_prompt_ if the variables are needed to be insert manually wihout save in the _vars_ file after run the ansible


